import React,{useEffect,useState} from 'react';
import DataTable from 'react-data-table-component';
import axios from 'axios'
import {API} from '../../config'

const columns =[
    {
        name: 'ID',
        selector: '_id',
        sortable: true
    },
    {
        name: 'Name',
        selector: 'name',
        sortable: true
    }
]


export default function ListProduct(){

    const [title, setTitle] = useState()

    useEffect(() => {
        console.log(API)
        async function fetchData() {
        const response = await axios.get(API+"/api/title")
        setTitle(response.data)
        }

        fetchData();
        //console.log(JSON.stringify(title)) -> COMO IMPRIMIR TITLE EN CONSOLE? FOREACH?
    }, [])

    return(
        <div>
            <DataTable
            columns={columns}
            title="List of Titles"
            data={title}
            >
            </DataTable>
        </div>
    );
}
import React from 'react';
import Form from '@rjsf/bootstrap-4';
import axios from 'axios'
import {API} from '../../config'

export default function FormProduct(){

  const onSubmit = async ({formData},e) => {
      
    await axios.post(API+"/api/title", formData,{
      headers:{
        "Content-Type": "application/json",
      }
    });
  }

  const schema = {
    title: "Registro de clientes/alumnos",
    properties: {
      "name": { "type": "string" },
    }
  };

  return(
      <Form schema={schema} onSubmit={onSubmit} >
            <div>
      <button type="submit" >Submit</button>
      <button type="button">Cancel</button>
    </div>
      </Form>
      
  )
}
import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import ListTitle from './component/title/ListTitle';
import CreateTitle from './component/title/CreateTitle';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/title' component={CreateTitle}></Route>
        <Route path='/titles' component={ListTitle}></Route>        
      </Switch>
  </Router>
  );
}

export default App;
